# Compiling
```docker build -t ticker-api:latest .```

# Running
1. Edit .env in this directory to alter any values you desire  
2. Launch the container with:  
```docker run -it --env-file=.env -p 5000 ticker-api:latest```

# Testing
``` curl http://localhost:5000/ ```  
```{"Meta Data":{"1. Information":"Daily Prices (open, high, low, close) and Volumes"...```

# Endpoints

| Path | Description |
| --- | --- |
| / | Implements the requested exercise, data returned is application/json |
| /metrics | Provides metrics regarding the performance of the app |
| /health | Responds to health-checks from the orchestration layer |

