#!/usr/bin/env python3
import sys
if not sys.version_info >= (3, 6):
    sys.exit("You must run at least version 3.6 of Python. Found: %s\n" % sys.version_info)

import logging, os, signal, time, numpy, requests, ciso8601

from flask import Flask, jsonify, make_response
from flask_restful import Resource, Api
from prometheus_flask_exporter import RESTfulPrometheusMetrics
from pythonjsonlogger import jsonlogger
from datetime import datetime

app = Flask(__name__)
api = Api(app)

metrics = RESTfulPrometheusMetrics(app, api)
upstream_market_data = {}

# Take a dictionary of data, look for specific keys to determine data age
# Inputs:
#  data      : A dictionary containing the keys ['Meta Data']['3. Last Refreshed'] with a value meeting ISO8601
#  threshold : Time (in seconds) after which we'll consider the dictionary stale 
#
# Outputs:
#   Boolean True  if the data is stale or not present in the dictionary
#   Boolean False if we found valid data in the dictionary and it appears to be current
#
# Errors:
#  If we're unable to convert Last Refreshed to a timestamp we'll exit the program abruptly.
#
def is_market_data_stale(data, threshold=86400):
    try:
        _meta = data['Meta Data']

        # Format: "2022-04-29 16:00:01"
        _last_refreshed = _meta['3. Last Refreshed']
    except (KeyError):
        return True

    try:
        # This is a safe way to convert our provided timestamp since it could change at any time
        _refreshed_timestamp = ciso8601.parse_datetime(_last_refreshed)
    except:
        logger.critical(f"Failed to parse {_last_refreshed} time into a date string: {e}")
        raise

    # Convert current time and provided time to unix time
    _refreshed_ts = time.mktime(_refreshed_timestamp.timetuple())
    _now = datetime.timestamp(datetime.now())

    # If our data is more than threshold seconds old (86400 by default) instruct the caller the data is stale
    if _refreshed_ts - _now > threshold:
        return True

    # We have non-stale data in our dictionary
    return False


# Make a request to our outside provider for API Key Data
# Inputs:
#   None
#
# Outputs:
#   A json object containing the entire reply from the remote API
#
# TODO:
#   Check actual response codes from Alphavantage.co. Are we being throttled? Convey that to our client if so

def fetch_market_data():
    try:
        logger.info(f"Attempting to fetch market data for symbol {SYMBOL}...")
        _resp = requests.get(f"https://www.alphavantage.co/query?apikey={APIKEY}&function=TIME_SERIES_DAILY&symbol={SYMBOL}")
        _last_refreshed = _resp.json()['Meta Data']['3. Last Refreshed']
        logger.debug(f"Fetched market data last updated: {_last_refreshed}")
        return _resp.json()
    except Exception as e:
        logger.critical(f"Error fetching market data {e}")
        raise

# Take a list of floats and average them, rounding to 4 decimal places by default
# Inputs:
#   list_of_floats  : An unordered list of floats
#
# Outputs:
#   A float rounded to the requested decimal places
#
def average_list(list_of_floats):
    _x = float(sum(list_of_floats) / len(list_of_floats))
    return f'{_x:.4f}'

# Root Handler for parsing requests made to /
#
# Inputs:
#  Resource: Passed from Flask RESTful
#
# Outputs:
#   None
class root_handler(Resource):
    def get(self):
        global upstream_market_data

        # Fetch our market data if it's non-existent or stale.
        try:
            if is_market_data_stale(upstream_market_data):
                upstream_market_data = fetch_market_data()
        except Exception as e:
            logger.critical(f"Failed to update market data. Error: {e}")
            return make_response({'error': 'Upstream data is currently unavailable'}, 503)

        # Verify we have the keys we expect to.
        try:
            time_series = upstream_market_data["Time Series (Daily)"]
        except KeyError as e:
            logger.critical(f"Data in the upstream_market_data dictionary doesn't look right.")
            return make_response({'error': 'Failed to fetch valid data from the remote API'}, 503)

        # Build our output dictionary
        output = {}
        output["Meta Data"] = upstream_market_data["Meta Data"]
        output["Time Series (Daily)"] = {}
        _days = 0
        _closing_prices_list = numpy.zeros(0)
        _highest_price = 0.0
        _lowest_price = 9999999.0
        _total_volume = 0

        # Walk each day in our time_series list, sorted newest to oldest record
        # Figure the closing price averages, and the highest closing price over the NDAYS period
        for day in sorted(time_series, reverse=True):
            if _days >= NDAYS:
                continue
            else:
                _closing_price = float(time_series[day]['4. close'] or None)
                if _closing_price > _highest_price:
                    _highest_price = _closing_price
                if _closing_price < _lowest_price:
                    _lowest_price = _closing_price
                _total_volume = _total_volume + int(time_series[day]['5. volume'])
                _closing_prices_list = numpy.append(_closing_prices_list, _closing_price)
                output["Time Series (Daily)"][day] = time_series[day]
                _days = _days + 1

        # We're only doing strings here so this data looks the same as the rest.
        output["Meta Data"]["6. Average Closing Price"] = f"{average_list(_closing_prices_list)}"
        output["Meta Data"]["7. High Closing Price"] = f"{_highest_price:.4f}"
        output["Meta Data"]["8. Lowest Closing Price"] = f"{_lowest_price:.4f}"
        output["Meta Data"]["9. Trading volume observed"] = f"{_total_volume}"

        return make_response(jsonify(output), 200)

# Health check handler responds 200 OK, That's all.
class health_check(Resource):
    def get(self):
        return {}, 200

# Logging and metrics setup, fetching and validating environment variables, and handling requests
if __name__ == '__main__':
    logger = logging.getLogger()

    logHandler = logging.StreamHandler()
    formatter = jsonlogger.JsonFormatter('%(asctime)s %(levelname)s %(funcName)s %(lineno)s %(message)s')
    logHandler.setFormatter(formatter)
    logger.addHandler(logHandler)
    logger.setLevel(os.getenv("LOG_LEVEL") or "INFO")
    logger.error(f"Log level set to: {logging.getLevelName(logger.getEffectiveLevel())}")

    # Grab our environment variables and do some simple sanity checking
    SYMBOL = os.getenv("SYMBOL")
    if SYMBOL == None:
        logger.error(f"You must set the SYMBOL environment variable to a valid stock ticker")
        raise SystemExit(1)
    if len(SYMBOL) > 5 or len(SYMBOL) < 2:
        logger.error(f"Symbols are between 2 and 5 characters on the stock market. You provided {SYMBOL}.")
        raise SystemExit(1)

    APIKEY = os.getenv("APIKEY")
    if APIKEY == None or len(APIKEY) > 45 or len(APIKEY) < 5:
        logger.error(f"You must set the APIKEY environment variable to a valid APIKEY for use with Alphavantage.co")
        raise SystemExit(1)

    if os.getenv("NDAYS") == None:
        logger.error(f"You must set the NDAYS environment variable to an integer between 1 and 999")
        raise SystemExit(1)
    else:
        NDAYS = int(os.getenv("NDAYS"))
        if (NDAYS > 999 or NDAYS < 1):
            logger.error(f"You must set the NDAYS environment variable to an integer between 1 and 999")
            raise SystemExit(1)

    api.add_resource(root_handler, '/')
    api.add_resource(health_check, '/health')
    app.run(debug=False, host='0.0.0.0')
