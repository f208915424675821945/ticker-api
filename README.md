# Hello ForgeRock!

Thank you for the opportunity to interview!

I'm not a great developer, I come from a SA background. I appreciate feedback on my Python code to help me become better.

I'd like to note that I did attempt this example in Golang but didn't get as far as I wanted in the time alotted, so I dropped it and
hammered this out in a couple hours in Python.

Here's the directory structure:   

| Path | Contents |
|---|---|
| manifests/ | Kubernetes-compatible manifests |
| docker/ | The ticker-api itself and how to build it |
| config/ | Dependencies for grafana and prometheus |

For funsies, you can run the included docker compose file to build and deploy this to your local machine, as well as a copy of Prometheus and Grafana.  
I've included a stock, simple dashboard, that displays metrics associated with the python app itself.

My focus is around telemetry and in this case I spent more time working code than I did refining telemetry, but I'd be happy to talk about it at length.


PS: My running copy of this code exercise is at https://ticker-api.lab.teki.sh/ -- This runs on my home lab microk8s infrastructure.  
Thanks again!

David
